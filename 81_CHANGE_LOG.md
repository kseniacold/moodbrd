---

# CS81 final project change log

---

### Complete list of the files worked on: 
+ ##### Front-end (```/app``` folder) created within Angular 2 framework. Language used TypeScript compiled to JavaScript:
  * ```/auth/81_login.component.ts``` Login component provides single page application functionality to redirect visitor to a corresponding view after logging in. 
  * ```/models/81_user.model.ts``` complete User class with accessor and mutators methods.
  * ```/posts/81_post-list.component.ts``` PostList component: 
    + Gets data from the server and displays existing posts and boards (two types of future envisioned user generated content).
    + Allows to create new board or post (corresponding record in the database is being generated).
    + Allows to delete board or post (corresponding item is being marked as "deleted" in the database, will not show anymore on the list).
  * ```/profile/81_profile.component.ts```  Profile component:
    + Gets current user profile from the server and displays name and user picture.
    + Allows to Log out.
    + Expands and collapses sidebar to open Settings menu view.
    + Listens to profile-settings component for emitting ```onCancel``` event. Handles an event by collapsing the sidebar.
  * ```/profile/81_profile-settings.component.ts``` ProfileSettings component:
    + Gets current user profile and binds the data to the form.
    + Displays current user profile image.
    + Allows for uploading a new image and immediately displays preview.
    + Displays and interactive form with required and optional fields to change user's profile settings.
    + ```OnSubmit``` sends new data to the server, receives an updated user back and displays a confirmation.
    + When clicking on the ```Cancel``` button emits ```onCancel``` event to be handled by the ProfileComponent.
    
  * ```/services/81_post.service.ts``` Post service:
    + Sends POST requests to the server to create a new board/post, delete post/board.
    + Sends GET request to get user's posts from the server.
  * ```/services/81_sidr.service.ts``` Provides the glue between Angular 2 app and SIDR JQuery plugin used to animate the sidebar. 
  * ```/services/81_update-profile.service.ts``` UpdateProfile service:
    + Sends POST request to the server with ```multipart/form-data``` (image and JSON string) to update user's profile.
  

---

+ ##### Back-end (```/moodbrd_api``` folder) uses Express.js framework for Node.js application, MongoDB and Mongoose library for database. Language used - JavaScript.
  * ```/models/81_post_schema.js``` database schema for Post class
  * ```/models/81_user_schema.js```  database schema for User class
  * ```/controllers/81_post.js``` handles requests related to posts creating and deleting:
    + ```addPost()``` function adds a reference to the post into user's "posts" property.
    + ```getUserPosts()``` gets posts by user with a provided callback function.
    +  ```setTitle()``` function sets the title for newly created Posts or Boards.
    + ```deletePost()```  function sets post's status to "deleted".
  * ```/controllers/81_profile.js``` :
    + ```profileUpdate()``` method handles client's request by updating user's information in the database and sending an updated user back to the client.
    + ```profileRead()``` method handles client's request by reading user's record in the database and sending corresponding response. 
  * ```/routes/81_uploads.js``` template file for handling future routes related to uploads.
  
