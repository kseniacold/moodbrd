$(document).ready(function() {
  var form,

    // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
    emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    name = $( '#signupName' ),
    username = $('#signupUsername'),
    email = $( '#signupEmail' ),
    password = $( '#signupPassword' ),
    passwordAgain = $('#signupPasswordAgain'),
    allFields = $( [] ).add( name ).add( email ).add( password ).add( username ).add( passwordAgain ),
    tips = $( '.validateTips' ),
    signupModal = $( '#signupModal' ),
    link = $('#signUp');

  function updateTips( t ) {
    tips
      .text( t )
      .addClass( "ui-state-highlight" );
    setTimeout(function() {
      tips.removeClass( "ui-state-highlight", 1500 );
    }, 500 );
  }

  function checkLength( o, n, min, max ) {
    if ( o.val().length > max || o.val().length < min ) {
      o.addClass( "ui-state-error" );
      updateTips( "Length of " + n + " must be between " +
        min + " and " + max + "." );
      return false;
    } else {
      return true;
    }
  }

  function checkPasswords( p1, p2, n ) {
    if ( p1.val() === p1.val() ) {
      return true;
    } else {
      p1.addClass('ui-state-error');
      p2.addClass('ui-state-error');
      updateTips( n );
      return false;
    }
  }

  function checkRegexp( o, regexp, n ) {
    if ( !( regexp.test( o.val() ) ) ) {
      o.addClass( "ui-state-error" );
      updateTips( n );
      return false;
    } else {
      return true;
    }
  }

  function addUser() {
    var valid = true;
    allFields.removeClass( "ui-state-error" );

    valid = valid && checkLength( name, "name", 2, 100 );
    valid = valid && checkLength( username, "username", 2, 40 );
    valid = valid && checkLength( email, "email", 6, 80 );
    valid = valid && checkLength( password, "password", 5, 30 );

    valid = valid && checkPasswords( password, passwordAgain, "Passwords don't match" );

    valid = valid && checkRegexp( username, /^[a-z]([0-9a-z_\s])+$/i, "Username must consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
    valid = valid && checkRegexp( email, emailRegex, "eg. user@email.com" );

    if ( valid ) {
      console.log('Success!\n' + name.val() + '\n' + username.val() + '\n' + email.val() + '\n' +  password.val() + '\n' +  passwordAgain.val() );
      $.modal.close();
    }
    return valid;
  }

  link.click(function(event) {
    signupModal.css({
      visibility: 'visible',
      opacity: '1'
    });

    $(this).modal({
      fadeDuration: 250
    });
    return false;
  });


  form = $('#form' ).on( 'submit', function( event ) {
    event.preventDefault();
    addUser();
  });

});
