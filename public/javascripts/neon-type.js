$(document).ready(function() {

  var color;

  var gradient = {
    primary_color: color
  }

  var inputColor = $('input[name="color"]');
  color = inputColor.val();

  inputColor.change(function() {
    color = inputColor.val();
    apply();
  });



  /* ============== Conversion Functions ========================= */

  function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
  }

  // Converts an RGB color value to HSL.
  // accepts parameters
  // r  Object = {r:x, g:y, b:z}
  // OR
  //r, g, b

  function rgbToHsl(r, g, b) {
    if (arguments.length === 1) {

      var argObj = r;
      r = argObj.r;
      g = argObj.g;
      b = argObj.b;

    }
   	r /= 255, g /= 255, b /= 255;

   	var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max == min) {
        h = s = 0; // achromatic
    }
    else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max) {
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }

        h = parseInt( h / 6 * 360 );
        s = parseInt(s * 100);
        l = parseInt(l * 100);
    }

    return {
        h: h,
        s: s,
        l: l
    };
  }

    //Converts an HSL color value to RGB.
    // accepts parameters
    // h  Object = {h:x, s:y, l:z}
    // OR
     //h, s, l

  function hslToRgb(h, s, l) {
    if (arguments.length === 1) {

      var argObj = h;
      h = argObj.h;
      s = argObj.s;
      l = argObj.l;

    }
  	h /= 360, s /= 100, l /= 100;
    var r, g, b;

    function hue2rgb(p, q, t) {
        if(t < 0) t += 1;
        if(t > 1) t -= 1;
        if(t < 1/6) return p + (q - p) * 6 * t;
        if(t < 1/2) return q;
        if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
        return p;
    }

    if(s === 0) {
        r = g = b = l; // achromatic
    }
    else {
        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    r = parseInt(r * 255);
    g = parseInt(g * 255);
    b = parseInt(b * 255);

    return {  r: r,
              g: g,
              b: b
    };
  }


  function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
  }

/* ============== End Conversion Functions ========================= */

  function createStops(color, obj) {
    obj.stop_1 = shadeColor(color, 0.63);;
    obj.stop_2 = shadeColor(color, 0.38);
    obj.stop_3 = shadeColor(color, 0.13);
    obj.stop_4 = shadeColor(color, -0.13);
    obj.stop_5 = shadeColor(color, -0.3);
    obj.stop_6 = shadeColor(color, -0.5);
  }

  function shadeColor(color, percent) {
    var f=parseInt(color.slice(1),16),
        t=percent<0?0:255,
        p=percent<0?percent*-1:percent,
        R=f>>16,
        G=f>>8&0x00FF,
        B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
  }

  function getNeonColor(color){
    var initColor = {};

    initColor.rgb = hexToRgb(color);
    initColor.hsl = rgbToHsl( initColor.rgb);


    var neonColor = Object.create(initColor);
    if ( initColor.hsl.l > 90){
      neonColor.hsl.l = 90;
      neonColor.hsl.s = 100;
      neonColor.rgb = hslToRgb(neonColor.hsl);

      return 'rgba(' + neonColor.rgb.r + ', ' + neonColor.rgb.g + ', ' + neonColor.rgb.g + ', 0.45)';

    } else {
      neonColor.hsl.l = 80;
      neonColor.hsl.s = 100;
      neonColor.rgb = hslToRgb(neonColor.hsl);
      neonColor.hex = rgbToHex(neonColor.rgb.r, neonColor.rgb.g, neonColor.rgb.b);

      return neonColor.hex;
    }


  }


  // Apply Colors to elements

  function apply(){
    createStops(color, gradient);
    var neonColor = getNeonColor(color);

    var section = $('#neon');
    var inputTxt = $('.neon > input');

    var mozString = '-moz-radial-gradient(center, ellipse cover, ' + gradient.stop_1 + ' 0%, ' + gradient.stop_2 + ' 23%, ' + gradient.stop_3 +' 47%, ' + gradient.stop_4 + ' 67%, ' + gradient.stop_5 + ' 83%, ' + gradient.stop_6 + ' 100%)';
    var webKString = '-webkit-radial-gradient(center, ellipse cover, ' + gradient.stop_1 + ' 0%, ' + gradient.stop_2 + ' 23%, ' + gradient.stop_3 +' 47%, ' + gradient.stop_4 + ' 67%, ' + gradient.stop_5 + ' 83%, ' + gradient.stop_6 + ' 100%)';
    var string = 'radial-gradient(ellipse at center, ' + gradient.stop_1 + ' 0%, ' + gradient.stop_2 + ' 23%, ' + gradient.stop_3 +' 47%, ' + gradient.stop_4 + ' 67%, ' + gradient.stop_5 + ' 83%, ' + gradient.stop_6 + ' 100%)';
    var ieString = 'filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="' + gradient.stop_1 + '", endColorstr="' + gradient.stop_6 + '",GradientType=1 )'

    section.css({
      /*  'background': gradient.primary_color, */
      'background': string
    });

    inputTxt.css({
      'text-shadow': '1px 1px 6px ' + neonColor + ', -1px -1px 6px ' + neonColor + ', 1px -1px 6px ' + neonColor + ', -1px 1px 6px ' + neonColor
    });
  }

  apply();


});
