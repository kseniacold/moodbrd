$(document).ready(function() {

  var grid = $('.grid');

  grid.imagesLoaded( function(){

    setImageSize();
    grid.packery({
      // options
      itemSelector: '.grid-item',
      gutter: 0
    });

    // make all grid-items draggable
    grid.find('.grid-item').each( function( i, gridItem ) {
      var draggie = new Draggabilly( gridItem );
      // bind drag events to Packery
      grid.packery( 'bindDraggabillyEvents', draggie );
    });

  });

  $(window).resize( setImageSize );

  function setImageSize(){
    var imagesItems = $('.grid-item');
    var images = $('.grid-item img');

    var winWidth = $(window).width();
    var imageWidth = winWidth / 6 ;

    imagesItems.css({
      "width": imageWidth
    });

    images.css({
      "width": imageWidth
    });
  }

});
