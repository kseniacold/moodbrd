$(document).ready(function()
{
  var input = $('.header .search__input').eq(0);

  input.focus( function()
  {
    var searchBox = $(this).parents('.search');
    searchBox.animate(
    {
      width: '100%'
    },
    {
      duration: 'slow',
      easing: 'swing'
    });
    //end animate
  });
  //end on focus

  input.focusout( function()
  {
    var searchBox = $(this).parents('.search');
    searchBox.animate(
    {
      width: '44%'
    },
    {
      duration: 'slow',
      easing: 'swing'
    });
    //end animate
  });
  //end off focus

});
