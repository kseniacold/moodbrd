$(document).ready(function() {
  var form,
      signinModal = $( '#signinModal' ),
      link = $('#signIn');


  link.click(function(event) {
    signinModal.css({
      visibility: 'visible',
      opacity: '1'
    });

    $(this).modal({
      fadeDuration: 250
    });
    return false;
  });

});
