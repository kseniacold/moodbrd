var express = require('express');
var app = express();
var path = require('path');
var router = express.Router();



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express yourself in a visual way' });
});

//TODO: remove this
/* GET angular page. */
router.get('/angular', function(req, res, next) {
  res.sendFile( path.join(__dirname, '../views', 'index-angular.html') );
});

/* GET playground demo page. */
router.get('/playground-demo', function(req, res, next) {
  res.render('playground-demo', { title: 'Express yourself in a visual way' });
});

//TODO: remove this
/* GET merge angular page. */
router.get('/merge-angular', function(req, res, next) {
  res.render('merge-angular-test', { title: 'Express yourself in a visual way' });
});

//TODO: remove this after complete authentication done - this is an old profile page rendered with pug
/* GET profile page. */
/* router.get('/profile', function(req, res, next) {
  res.render('profile', { title: 'Express yourself in a visual way' });
}); */

/* GET routes for angular templates */
router.get('/template/*', function(req, res) {
  //TODO: remove log
  //console.log(req.params);
  try {
    res.render( req.params[0]);
  } catch(e) {
    console.log('render error', e);
  }
});

//TODO: remove this test page after authentication is done
/* GET merge angular page. */
router.get('/header', function(req, res, next) {
  res.render('header__landing', { title: 'Express yourself in a visual way' });
});


/* ======= Catch Requests to return Index page for Angular app ==========*/

/* GET profile page. */
router.get('/profile', function(req, res, next) {
  res.render('index', { title: 'Express yourself in a visual way' });
});

module.exports = router;
