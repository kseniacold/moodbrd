
# ![](logo.png)

##### Moodbrd web application is thought to be a place for sharing vusual ideas in an interactive way.

###### Graphic design for the project is created by *Sofia* *Saprykina*

###### Web development: *Ksenia* *Koldaeva*

---

# Workflow & Environment:

![](git.png) [Git - version control system](https://git-scm.com/)

![](node.png)  [Node.js - server-side JavaScript environment](https://nodejs.org/en/)

![](npm.png)  [npm - package manager for Node.js](https://www.npmjs.com/)

![](bower_1.png)  [Bower - package manager](https://bower.io/)


---

## Back-end key technologies

### Express

[expressjs.com](http://expressjs.com/en)

It's a server side framework for Node.js that provides a layer of abstraction from Node. Features that I used include :
+ Routing - providing web application end points - URI
+ Error handling - 404, 500 and such errors
+ Usage of template engine (Here used Pug)
+ Setting up middleware
+  Database integration

---

### MongoDB

![](mongo.png) [mongodb.com](https://docs.mongodb.com/)

Open source document oriented database that does not enforce schemas. Record in MongoDB is a document - data structure composed of field and value pairs.

```
{ 	
 name: "Amelie Poulin",
 username: "amelie",
 email: "amelie@email.com"
 }
```

---

## Front-end key technologies

### BEM - methodology (naming principles)

![](bem.png) [https://en.bem.info/](https://en.bem.info/)

Bem methodology helps creating extandable and maintainable markup by keeping it consistent. Main idea - make CSS selectors informative and clear.

BEM stands for block, element, modifier

---
+ A block name defines a namespace for elements and modifiers.

```
<div class="menu"></div>
```

+ The namespace defined by the name of a block identifies an element as belonging to the block. An element name is delimited by a double underscore.

```
<div class="menu"><span class="menu__item"></span></div>
```

---

+ The namespace defined by the name of a block identifies a modifier as belonging to that block or its element. A modifier name is delimited by a single underscore.

```
<div class="menu">
	<span class="menu__item"></span>
    	<span class="menu__item_hidden"></span>
</div>
```

---

### Pug

![](pug.png) [pugjs.org](https://pugjs.org)

Pug is a template engine for Node.js. Here is some Pug's features:
+ Allows to separate dynamic HTML from the static layout.
+ Allows to maintain modular structure of the project. E.g. I create separate files for the footer and the header of the page that will be added to the final page HTML at compile time.  
+ Provides minimal logic to HTML e.g. if else statements and JavaScript insertions

---

Pug does not have open and closing < > tags. Instead it uses indentation. Here is Pug source code example from my project


```
header.header
  .container
    //Header logo start
    .header__logo
      a(
        href='#'
        class='logo'
        )
        img.logo__svg(
          src='/svg/logotype__neon_resized.svg'
          alt='Moodbrd Logotype'
          )
```
---

**compiles to:**

```
<header class="header">
  <div class="container">
    <!--Header logo start-->
    <div class="header__logo">
      <a class="logo" href="#">
        <img class="logo__svg" src="/svg/logotype__neon_resized.svg" alt="Moodbrd Logotype">
      </a>
    </div>  
  </div>
</header>
```

---

### LESS - CSS pre-processor

![](less.png) [lesscss.org](http://lesscss.org/)

LESS extends CSS language, adding features that allow variables, mixins and functions, making CSS that is more maintainable and extendable.

---

###### Variables with LESS:

```
@nice-blue: #5B83AD;
@light-blue: @nice-blue + #111;

#header {
  color: @light-blue;
}

```

**outputs**
```
#header {
  color: #6c94be;
}

```

---

###### Mixins with parametrs

```
//Mixin declaration
.border-radius(@radius: 5px) {
  -webkit-border-radius: @radius;
     -moz-border-radius: @radius;
          border-radius: @radius;
}

//Using mixin
#header {
  .border-radius(4px);
}

```

---

### LESSHAT - LESS mixin library

![](lesshat.png) [lesshat](https://github.com/madebysource/lesshat)

Very useful library for making CSS browser compatible by making sure you're using right prefixes in CSS rules.

E.g.

```
div {
 .align-items(flex-start);
}

// Result
div {
 -webkit-box-align: flex-start;
 -moz-box-align: start;
 -webkit-align-items: start;
 -ms-flex-align: flex-start;
 align-items: flex-start;
}

```
---

### SVG - Scalable Vector Graphics

SVG is a platform for two-dimensional graphics. It has XML-based file format and can be generated via Adobe Illustrator.

+ SVG graphic scales seamlessly for any format in opposite to rather graphic which has its' maximum resolution. Then, pixelization will be visible.

+ All graphics for Moodbrd are in .svg format including, logo, services icons, profile picture placeholder.

---

## JQuery libraries used in the project


### JQuery Modal


 [jquery-modal](http://jquerymodal.com/) For modal windows and pop ups.

 ### SIDR

  ![](sidr.png) [SIDR](https://www.berriart.com/sidr/) JQuery plugin for sliding menu.

---

 ### Packery

 ![](packery.png) [Packery](http://packery.metafizzy.co/) JQuery plugin for making Masonry grids and Draggrable layouts.

 ![](packery-grid.png)

 ---

  ###  Draggabilly

  [Draggabilly](http://draggabilly.desandro.com/) JQuery plugin for making Mood board grid elements draggable!

  ---


  ###  Images Loaded

  [Images Loaded](http://imagesloaded.desandro.com/) Very useful plugin that allows to work with the images when they are loaded (Binds a callback function to image load event).

  ---

## More back-end libraries used in the project

###  Crypto

[Crypto](https://github.com/brix/crypto-js) - JavaScript library of crypto standards.

Is used to encrypt the password entered by used. The password is stored in a form of Salted hash.

---

###### Here is how the hash looks like for a trivial password:

cdf29d956ab6864c2b3281250697056c08a51b7c6fce63aedb6a77b3a1eb47e031bdab158ba6db14f9814c58ccc1c4e81fea83209b74acf799ee72abb4ea33dc59b327c42d2b5d323d4c0b251619d891f2e25e7e513d5a2b56826ad36ae5d9a2ee89f059430d334968905691c254ec942210c0d01fbe04ef119c31d0122a6bbb5ca4b957c9ea87e85c861ac1fc40e340c81825eeb14cd1ed5a978670846fb09b0eaf5130cb4657c22b2c3a6535679fb6345933a4b0c33f6dd935dca571a3294ce9dcc689992dc67265c17085d62a6bab851b68813a315309363cdcbbefa50da1b4b6d798de6f7940a06ea4b07bdda39a10171ecd882a132ac9849e6d284401ba


---

###  Passport

![](passport.png) [Passport](http://passportjs.org/docs) - is authentication middleware for Node.

Designed to authenticate requests. Allows to create multiple "strategies" for different forms of Authentication, e.g. Facebook, Twitter authentication and many more.

---

###  Mongoose

![](mongoose.png) [Mongoose](http://mongoosejs.com/) - extends MondoDB. It allows schema-based modeling the application data.

---

Example of Mongoose Schema

```
var userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  encryptedPassword: String,
  salt: String
});

```

---

## Neon type - Digital Playground Page

###### At the Neon Type playground page the user can choose a background color - which will be turned to a gradient background, and can type text, that will have a neon light glow effect.

In order to create a neon light glow effect a text-shadow CSS property is applied to the text.

The thing is that text-shadow color must be a corresponding neon shade of the basic background color.

E.g. for the base background ![](background-color.png) corresponding text shadow neon color will be ![](text-shadow.png)

---

The main idea was to create such a script that by giving it a simple base color it will generate background gradient and a corresponding neon shade that will be applied as text-shadow.


The JavaScript ```neon-type.js``` contains 4 color conversion functions:

```
hexToRgb();

rgbToHsl();

  // Converts an RGB color value to HSL.
  // accepts parameters
  // r  Object = {r:x, g:y, b:z}
  // OR
  //r, g, b

```

---

```
hslToRgb();

    //Converts an HSL color value to RGB.
    // accepts parameters
    // h  Object = {h:x, s:y, l:z}
    // OR
     //h, s, l

rgbToHex();

  // Converts an RGB color value to HEX.
  // accepts parameters
  //r, g, b

```

---
To find out a neon shade of a color it is the best to convert it to HSL color space because the lighting channel is separate you deal with heus only.

![](hsl.png)
![](rgb.png)



---

To find a correct shade of for the neon color, initial color is converted to RGB, then -> HSl.

At this point the transformation is made - Hue is preserved, Lightness is set to 80 and Saturation is set to 100 for any color - this way we're getting neon shade of a corresponding color.

Next, color is converted back to RGB and HEX.

---

### Thank you,
### I hope you enjoyed the presentation!

👾  👾  👾  
