import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { Routing, RoutedComponents } from './app.routing';

import { AppComponent }  from './app.component';
import { HeaderComponent }  from './header/header.component';

import { LoginComponent }  from './auth/81_login.component';
import { RegisterComponent }  from './auth/register.component';

import { ProfileComponent }  from './profile/81_profile.component';
import { CollapsedMenuComponent }  from './profile/profile-col.component';
import { PostListComponent } from './posts/81_post-list.component';
import { ProfileSettingsComponent} from './profile/81_profile-settings.component';

// Globals
import { PostConfig } from './globals/post.config';
import { UserConfig } from './globals/81_user.config';

import { AuthService } from './services/authentication.service';
import { AuthGuard } from './services/auth-guard.service';
import { GetProfile } from './services/get-profile.service';
import { PostService } from './services/81_post.service';
import { UpdateProfile } from './services/81_update-profile.service';

import { PackeryService } from './services/packery.service';
import { SidrService } from './services/81_sidr.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    Routing,
    ReactiveFormsModule
   ],
  declarations: [
    AppComponent,
    RoutedComponents,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    CollapsedMenuComponent,
    ProfileSettingsComponent,
    PostListComponent
  ],
  providers: [
    PostConfig,
    UserConfig,
    AuthService,
    AuthGuard,
    GetProfile,
    PostService,
    UpdateProfile,
    PackeryService,
    SidrService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
