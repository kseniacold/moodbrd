import { Component } from '@angular/core';
import { PackeryService } from './services/packery.service';
import { Router } from '@angular/router';

import { SidrService } from './services/81_sidr.service';
import { AuthService } from './services/authentication.service';


@Component({
  selector: 'moodbrd-app',
  templateUrl: '/template/moodbrd-app',
})

export class AppComponent  {
  showProfileMenu: boolean = false;
  constructor( private packery: PackeryService, private auth: AuthService, private router: Router, private sidr: SidrService) {}

  /* Angular lifecycle hooks */
  ngOnInit() {
    if (this.auth.isLoggedIn() ) {
      this.showProfileMenu = true;
    }
  }

   ngAfterViewInit() {
     this.packery.packeryInit();
     this.sidr.sidrInit();
   }

}
