"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var packery_service_1 = require('./services/packery.service');
var router_1 = require('@angular/router');
var _81_sidr_service_1 = require('./services/81_sidr.service');
var authentication_service_1 = require('./services/authentication.service');
var AppComponent = (function () {
    function AppComponent(packery, auth, router, sidr) {
        this.packery = packery;
        this.auth = auth;
        this.router = router;
        this.sidr = sidr;
        this.showProfileMenu = false;
    }
    /* Angular lifecycle hooks */
    AppComponent.prototype.ngOnInit = function () {
        if (this.auth.isLoggedIn()) {
            this.showProfileMenu = true;
        }
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        this.packery.packeryInit();
        this.sidr.sidrInit();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'moodbrd-app',
            templateUrl: '/template/moodbrd-app',
        }), 
        __metadata('design:paramtypes', [packery_service_1.PackeryService, authentication_service_1.AuthService, router_1.Router, _81_sidr_service_1.SidrService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map