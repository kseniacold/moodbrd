import { Component } from '@angular/core';
import { AuthService } from '../services/authentication.service';


@Component({
  selector: 'header-component',
  templateUrl: '/template/header'
})

export class HeaderComponent  {
  showSignUp: boolean = false;
  showSignIn: boolean = false;
  isLoggedIn = this.authService.isLoggedIn();
  currentUser = this.authService.currentUser();

  constructor( private authService: AuthService ) {}
}
