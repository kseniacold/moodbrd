export class Post {
  constructor(
    private _obj: Object,
    private __id?: string,
    private _creator?: string,
    private _type?: string,
    private _likes?: [{}],
    private _comments?: [{}],
    private _tags?: [{}],
    private _description?: string,
    private _title?: string,
    private _status?: string,
    private _updated?: string,
    private _allowCommentsFor?: string,
    private _featured?: boolean,
    private _collaborators?: [{}]
  ) {
    //Automatically populate an instance when data is passed
    for (let prop in _obj) this['_' + prop] = _obj[prop];
  }

  get id(): string {
    return this.__id;
  }

  get creator(): string {
    return this._creator;
  }

  get type(): string {
    return this._type;
  }

  get likes(): Array<Object> {
    return this._likes;
  }

  get comments(): Array<Object> {
    return this._comments;
  }

  get tags(): Array<Object> {
    return this._tags;
  }

  get description(): string {
    return this._description;
  }

  get title(): string {
    return this._title;
  }

  get status(): string {
    return this._status;
  }

  get updated(): string {
    return this._updated;
  }

  get allowCommentsFor(): string {
    return this._allowCommentsFor;
  }

  get featured(): boolean {
    return this._featured;
  }

  get collaborators(): Array<Object> {
    return this._collaborators;
  }


  /*

  public addLike(like:Like) {
    this._likes.push(like);
  }

  public addComment(comment:Comment) {
    this._comments.push(comment);
  }

  public addTag(tag:Tag) {
    this._tags.push(tag);
  }

  */

  set description(newDescription: string) {
    this._description = newDescription;
  }

  set title(newTitle: string) {
    this._title = newTitle;
  }

  set status(newStatus: string) {
    //TODO: check business logic for setter - restricted values
    this._status = newStatus;
  }

  set allowCommentsFor(newAllowCommentsFor: string) {
    //TODO: check business logic for setter - restricted values
    this._allowCommentsFor = newAllowCommentsFor;
  }

  set featured(newFeatured: boolean)  {
    this._featured = newFeatured;
  }

  /*
  public addCollaborator(collaborator:Collaborator) {
    this._collaborators.push(collaborator);
  }
  */

}
