"use strict";
var Post = (function () {
    function Post(_obj, __id, _creator, _type, _likes, _comments, _tags, _description, _title, _status, _updated, _allowCommentsFor, _featured, _collaborators) {
        this._obj = _obj;
        this.__id = __id;
        this._creator = _creator;
        this._type = _type;
        this._likes = _likes;
        this._comments = _comments;
        this._tags = _tags;
        this._description = _description;
        this._title = _title;
        this._status = _status;
        this._updated = _updated;
        this._allowCommentsFor = _allowCommentsFor;
        this._featured = _featured;
        this._collaborators = _collaborators;
        //Automatically populate an instance when data is passed
        for (var prop in _obj)
            this['_' + prop] = _obj[prop];
    }
    Object.defineProperty(Post.prototype, "id", {
        get: function () {
            return this.__id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "creator", {
        get: function () {
            return this._creator;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "type", {
        get: function () {
            return this._type;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "likes", {
        get: function () {
            return this._likes;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "comments", {
        get: function () {
            return this._comments;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "tags", {
        get: function () {
            return this._tags;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "description", {
        get: function () {
            return this._description;
        },
        /*
      
        public addLike(like:Like) {
          this._likes.push(like);
        }
      
        public addComment(comment:Comment) {
          this._comments.push(comment);
        }
      
        public addTag(tag:Tag) {
          this._tags.push(tag);
        }
      
        */
        set: function (newDescription) {
            this._description = newDescription;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "title", {
        get: function () {
            return this._title;
        },
        set: function (newTitle) {
            this._title = newTitle;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "status", {
        get: function () {
            return this._status;
        },
        set: function (newStatus) {
            //TODO: check business logic for setter - restricted values
            this._status = newStatus;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "updated", {
        get: function () {
            return this._updated;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "allowCommentsFor", {
        get: function () {
            return this._allowCommentsFor;
        },
        set: function (newAllowCommentsFor) {
            //TODO: check business logic for setter - restricted values
            this._allowCommentsFor = newAllowCommentsFor;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "featured", {
        get: function () {
            return this._featured;
        },
        set: function (newFeatured) {
            this._featured = newFeatured;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "collaborators", {
        get: function () {
            return this._collaborators;
        },
        enumerable: true,
        configurable: true
    });
    return Post;
}());
exports.Post = Post;
//# sourceMappingURL=post.model.js.map