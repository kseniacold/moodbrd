export class User {
  constructor(
    private _obj: Object,
    private __id?: string,
    private _encryptedPassword?: string,
    private _name?: string,
    private _username?: string,
    private _email?: string,
    private _posts?: Array<Object>,
    private _personalWebsite?: string,
    private _bio?: string,
    private _profileImage?: string,
    private _followsUsers?: Array<Object>,
    private _followedByUsers?: Array<Object>,
    private _blockedUsers?: Array<Object>,
    private _likedElements?: Array<Object>,
    private _allowCommentsFor?: string
  )
  {
    //Automatically populate an instance when data is passed
    for (let prop in _obj) this['_' + prop] = _obj[prop];
  }

  get id(): string {
    return this.__id;
  }

  get encryptedPassword(): string {
    return this._encryptedPassword;
  }

  get name(): string {
    return this._name;
  }

  get username(): string {
    return this._username;
  }

  get email(): string {
    return this._email;
  }

  get posts(): Array<Object> {
    return this._posts;
  }

  get personalWebsite(): string {
    return this._personalWebsite;
  }

  get bio(): string {
    return this._bio;
  }

  get profileImage(): string {
    return this._profileImage;
  }

  get followsUsers(): Array<Object> {
    return this._followsUsers;
  }

  get followedByUsers(): Array<Object> {
    return this._followedByUsers;
  }

  get blockedUsers(): Array<Object> {
    return this._blockedUsers;
  }

  get likedElements(): Array<Object> {
    return this._likedElements;
  }

  get allowCommentsFor(): string {
    //TODO: check constraint
    return this._allowCommentsFor;
  }

  set name(newName: string) {
    this._name = newName;
  }

  set username(newUsername: string) {
    this._username = newUsername;
  }

  set email(newEmail: string) {
    this._email = newEmail;
  }

  set personalWebsite(newPersonalWebsite: string) {
    this._personalWebsite = newPersonalWebsite;
  }

  set bio(newBio: string) {
    this._bio = newBio;
  }

  set profileImage(newProfileImage: string) {
    this._profileImage = newProfileImage;
  }

  /*
  followUser(userId:string) {
    this._followsUsers.push(userId);
  }

  blockUser(userId:string) {
    this._blockedUsers.push(userId);
  }
  */

  set allowCommentsFor(newAllowCommentsFor: string) {
    this._allowCommentsFor = newAllowCommentsFor;
  }


}
