"use strict";
var RegisterUser = (function () {
    function RegisterUser(name, username, email, password, retypedPassword) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.retypedPassword = retypedPassword;
    }
    return RegisterUser;
}());
exports.RegisterUser = RegisterUser;
//# sourceMappingURL=register-user.model.js.map