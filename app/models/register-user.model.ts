export class RegisterUser {
  constructor(
    public name: string,
    public username: string,
    public email: string,
    public password: string,
    public retypedPassword: string
  ) {  }
}
