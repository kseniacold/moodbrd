"use strict";
var User = (function () {
    function User(_obj, __id, _encryptedPassword, _name, _username, _email, _posts, _personalWebsite, _bio, _profileImage, _followsUsers, _followedByUsers, _blockedUsers, _likedElements, _allowCommentsFor) {
        this._obj = _obj;
        this.__id = __id;
        this._encryptedPassword = _encryptedPassword;
        this._name = _name;
        this._username = _username;
        this._email = _email;
        this._posts = _posts;
        this._personalWebsite = _personalWebsite;
        this._bio = _bio;
        this._profileImage = _profileImage;
        this._followsUsers = _followsUsers;
        this._followedByUsers = _followedByUsers;
        this._blockedUsers = _blockedUsers;
        this._likedElements = _likedElements;
        this._allowCommentsFor = _allowCommentsFor;
        //Automatically populate an instance when data is passed
        for (var prop in _obj)
            this['_' + prop] = _obj[prop];
    }
    Object.defineProperty(User.prototype, "id", {
        get: function () {
            return this.__id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "encryptedPassword", {
        get: function () {
            return this._encryptedPassword;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (newName) {
            this._name = newName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "username", {
        get: function () {
            return this._username;
        },
        set: function (newUsername) {
            this._username = newUsername;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (newEmail) {
            this._email = newEmail;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "posts", {
        get: function () {
            return this._posts;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "personalWebsite", {
        get: function () {
            return this._personalWebsite;
        },
        set: function (newPersonalWebsite) {
            this._personalWebsite = newPersonalWebsite;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "bio", {
        get: function () {
            return this._bio;
        },
        set: function (newBio) {
            this._bio = newBio;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "profileImage", {
        get: function () {
            return this._profileImage;
        },
        set: function (newProfileImage) {
            this._profileImage = newProfileImage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "followsUsers", {
        get: function () {
            return this._followsUsers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "followedByUsers", {
        get: function () {
            return this._followedByUsers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "blockedUsers", {
        get: function () {
            return this._blockedUsers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "likedElements", {
        get: function () {
            return this._likedElements;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "allowCommentsFor", {
        get: function () {
            //TODO: check constraint
            return this._allowCommentsFor;
        },
        /*
        followUser(userId:string) {
          this._followsUsers.push(userId);
        }
      
        blockUser(userId:string) {
          this._blockedUsers.push(userId);
        }
        */
        set: function (newAllowCommentsFor) {
            this._allowCommentsFor = newAllowCommentsFor;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
//# sourceMappingURL=81_user.model.js.map