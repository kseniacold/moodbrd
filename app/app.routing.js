"use strict";
var router_1 = require('@angular/router');
var _81_profile_component_1 = require('./profile/81_profile.component');
var appRoutes = [
    {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
    }
];
exports.Routing = router_1.RouterModule.forRoot(appRoutes);
exports.RoutedComponents = [_81_profile_component_1.ProfileComponent];
//# sourceMappingURL=app.routing.js.map