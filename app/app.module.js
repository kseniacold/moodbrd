"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var forms_2 = require('@angular/forms');
var http_1 = require('@angular/http');
var app_routing_1 = require('./app.routing');
var app_component_1 = require('./app.component');
var header_component_1 = require('./header/header.component');
var _81_login_component_1 = require('./auth/81_login.component');
var register_component_1 = require('./auth/register.component');
var _81_profile_component_1 = require('./profile/81_profile.component');
var profile_col_component_1 = require('./profile/profile-col.component');
var _81_post_list_component_1 = require('./posts/81_post-list.component');
var _81_profile_settings_component_1 = require('./profile/81_profile-settings.component');
// Globals
var post_config_1 = require('./globals/post.config');
var _81_user_config_1 = require('./globals/81_user.config');
var authentication_service_1 = require('./services/authentication.service');
var auth_guard_service_1 = require('./services/auth-guard.service');
var get_profile_service_1 = require('./services/get-profile.service');
var _81_post_service_1 = require('./services/81_post.service');
var _81_update_profile_service_1 = require('./services/81_update-profile.service');
var packery_service_1 = require('./services/packery.service');
var _81_sidr_service_1 = require('./services/81_sidr.service');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                http_1.JsonpModule,
                app_routing_1.Routing,
                forms_2.ReactiveFormsModule
            ],
            declarations: [
                app_component_1.AppComponent,
                app_routing_1.RoutedComponents,
                header_component_1.HeaderComponent,
                _81_login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                _81_profile_component_1.ProfileComponent,
                profile_col_component_1.CollapsedMenuComponent,
                _81_profile_settings_component_1.ProfileSettingsComponent,
                _81_post_list_component_1.PostListComponent
            ],
            providers: [
                post_config_1.PostConfig,
                _81_user_config_1.UserConfig,
                authentication_service_1.AuthService,
                auth_guard_service_1.AuthGuard,
                get_profile_service_1.GetProfile,
                _81_post_service_1.PostService,
                _81_update_profile_service_1.UpdateProfile,
                packery_service_1.PackeryService,
                _81_sidr_service_1.SidrService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map