import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuard } from './services/auth-guard.service';

import { ProfileComponent } from './profile/81_profile.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  }

];

export const Routing = RouterModule.forRoot(appRoutes);

export const RoutedComponents = [ProfileComponent];
