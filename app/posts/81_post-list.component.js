"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var get_profile_service_1 = require('../services/get-profile.service');
var _81_post_service_1 = require('../services/81_post.service');
var _81_user_model_1 = require('../models/81_user.model');
var post_model_1 = require('../models/post.model');
var PostListComponent = (function () {
    function PostListComponent(profileService, postService) {
        this.profileService = profileService;
        this.postService = postService;
        this.user = new _81_user_model_1.User({});
        this.posts = [];
        //Initial settings
        this.showAddPost = false;
    }
    //This executes
    PostListComponent.prototype.ngOnInit = function () {
        var self = this;
        self.profileService
            .getProfile()
            .subscribe(function (data) {
            self.user = new _81_user_model_1.User(data);
            //get user posts
            self.postService
                .getPosts(self.user)
                .subscribe(function (data) {
                data.forEach(function (obj) {
                    // Creates instance of Post class from JSON data received
                    var post = new post_model_1.Post(obj);
                    if (post.status !== "deleted") {
                        self.posts.push(post);
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }, function (error) {
            console.log(error);
        });
    };
    PostListComponent.prototype.createBoard = function (user) {
        var _this = this;
        this.postService.createBoard(user).subscribe(function (response) {
            var obj = response, post = new post_model_1.Post(obj);
            _this.posts.push(post);
        }, function (error) {
            console.log(error);
        });
    };
    PostListComponent.prototype.createPost = function (user) {
        var _this = this;
        this.postService.createPost(user).subscribe(function (response) {
            var obj = response, post = new post_model_1.Post(obj);
            _this.posts.push(post);
        }, function (error) {
            console.log(error);
        });
    };
    PostListComponent.prototype.deletePost = function (post) {
        var self = this;
        this.postService.deletePost(post).subscribe(function (response) {
            self.posts = [];
            self.postService
                .getPosts(self.user)
                .subscribe(function (data) {
                data.forEach(function (obj) {
                    // Creates instance of Post class from JSON data received
                    var post = new post_model_1.Post(obj);
                    if (post.status !== "deleted") {
                        self.posts.push(post);
                    }
                });
            }, function (error) {
                console.log(error);
            });
        }, function (error) {
            console.log(error);
        });
    };
    PostListComponent = __decorate([
        core_1.Component({
            selector: 'post-list',
            templateUrl: 'template/post_list'
        }), 
        __metadata('design:paramtypes', [get_profile_service_1.GetProfile, _81_post_service_1.PostService])
    ], PostListComponent);
    return PostListComponent;
}());
exports.PostListComponent = PostListComponent;
//# sourceMappingURL=81_post-list.component.js.map