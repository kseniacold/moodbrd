import { Component } from '@angular/core';
import { AuthService } from '../services/authentication.service';
import { GetProfile } from '../services/get-profile.service';
import { PostService } from '../services/81_post.service';

import { User } from '../models/81_user.model';
import { Post } from '../models/post.model';

@Component({
  selector: 'post-list',
  templateUrl: 'template/post_list'
})

export class PostListComponent {
  user:User = new User({});
  posts: Array<Post> = [];

  constructor(private profileService: GetProfile, private postService: PostService) {}

  //Initial settings
  showAddPost:boolean = false;

  //This executes
  ngOnInit(): void {
    let self = this;
    self.profileService
      .getProfile()
      .subscribe(
        data => {
           self.user = new User(data);
          //get user posts
           self.postService
               .getPosts(self.user)
               .subscribe(
                 data => {
                   data.forEach(function(obj:Object) {
                    // Creates instance of Post class from JSON data received
                    let post = new Post(obj);
                     if(post.status !== "deleted") {
                       self.posts.push(post);
                     }
                   });
                 },
                 error => {
                   console.log(error);
                 }
               );
        },
        error => {
          console.log(error);
        }
      );
  }

  createBoard(user:User) {
    this.postService.createBoard(user).subscribe(
      response => {
        let obj:Object = response,
            post:Post = new Post(obj);

        this.posts.push(post);
      },
      error => {
        console.log(error);
      }
    );
  }

  createPost(user:User) {
    this.postService.createPost(user).subscribe(
      response => {
        let obj:Object = response,
          post:Post = new Post(obj);

        this.posts.push(post);
      },
      error => {
        console.log(error);
      }
    );
  }

  deletePost(post:Post) {
    let self = this;
    this.postService.deletePost(post).subscribe(
      response  => {
        self.posts = [];
        self.postService
            .getPosts(self.user)
            .subscribe(
              data => {
                data.forEach(function(obj:Object) {
                  // Creates instance of Post class from JSON data received
                  let post = new Post(obj);
                  if(post.status !== "deleted") {
                    self.posts.push(post);
                  }
                });
              },
              error => {
                console.log(error);
              }
            );
      },
      error => {
        console.log(error);
      }
    );
  }
}
