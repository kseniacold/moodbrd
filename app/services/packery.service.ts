import { Injectable } from '@angular/core';
declare var $:any;
declare var  Draggabilly:any;

@Injectable()
export class PackeryService {

  setImageSize(){
    var imagesItems = $('.grid-item');
    var images = $('.grid-item img');

    var winWidth = $(window).width();
    var imageWidth = winWidth / 6 ;

    imagesItems.css({
      "width": imageWidth
    });

    images.css({
      "width": imageWidth
    });
  }

  packeryInit(){
    var grid = $('.grid');
    let self = this;
    grid.imagesLoaded( function(){

      self.setImageSize();
      grid.packery({
        // options
        itemSelector: '.grid-item',
        gutter: 0
      });

      // make all grid-items draggable
      grid.find('.grid-item').each( function( i:any, gridItem:any ) {
        var draggie = new Draggabilly( gridItem );
        // bind drag events to Packery
        grid.packery( 'bindDraggabillyEvents', draggie );
      });

    });

    $(window).resize( this.setImageSize );
  }
}
