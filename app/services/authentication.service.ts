import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { Http, Response }          from '@angular/http';
import { HttpModule, JsonpModule } from '@angular/http';

import { RegisterUser }    from '../models/register-user.model';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  constructor( private http: Http ) {}

  saveToken( res: Response ) {
    let body = res.json();
    localStorage.setItem('moodbrd_token', body.token );
  }

  getToken(){
    return localStorage.getItem('moodbrd_token');
  }

  isLoggedIn() {
    let token = this.getToken();
    let payload: any;

    if(token){
      payload = token.split('.')[1];
      payload = atob(payload);
      payload = JSON.parse(payload);

      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  currentUser() {
    if( this.isLoggedIn() ){
      let token = this.getToken();
      let payload:any = token.split('.')[1];
      payload = atob(payload);
      payload = JSON.parse(payload);
      return {
        email : payload.email,
        name : payload.name
      };
    }
  }

  register(user:any): Observable<RegisterUser>{
    return this.http.post('/users/register', user)
                    .map(this.saveToken)
                    .catch(this.handleError);
  }

  login( user:any ){
    return this.http.post('/users/login', user)
                    .map(this.saveToken)
                    .catch(this.handleError);
  }


  logout() {
    localStorage.removeItem('moodbrd_token');
  }

  private extractData(res: Response) {
    let body = res.json();
    console.log( body );
    return body.data || { };
  }

  private handleError (error: Response | any) {
      // TODO: check it out: In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
  }


}
