"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SidrService = (function () {
    function SidrService() {
    }
    SidrService.prototype.sidrInit = function () {
        var self = this;
        self.sidr = $('#sidr');
        self.menuOpen = $('#menuOpen');
        self.menuClose = $('#menuClose');
        self.sidr.css('display', 'block');
        self.menuOpen.sidr({
            side: 'right',
            displace: false,
            onOpen: function () {
                self.menuOpen.css('display', 'none');
            },
            onClose: function () {
                self.menuOpen.css('display', 'block');
            }
        });
        self.menuClose.sidr({
            side: 'right',
            displace: false,
            onOpen: function () {
                self.menuOpen.css('display', 'none');
            },
            onClose: function () {
                self.menuOpen.css('display', 'block');
            }
        });
    };
    SidrService.prototype.sidrExpand = function () {
        var self = this;
        self.sidr = $('#sidr');
        this.sidr.addClass('expanded');
    };
    SidrService.prototype.sidrCollapse = function () {
        var self = this;
        self.sidr = $('#sidr');
        this.sidr.removeClass('expanded');
    };
    SidrService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], SidrService);
    return SidrService;
}());
exports.SidrService = SidrService;
//# sourceMappingURL=81_sidr.service.js.map