"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var PackeryService = (function () {
    function PackeryService() {
    }
    PackeryService.prototype.setImageSize = function () {
        var imagesItems = $('.grid-item');
        var images = $('.grid-item img');
        var winWidth = $(window).width();
        var imageWidth = winWidth / 6;
        imagesItems.css({
            "width": imageWidth
        });
        images.css({
            "width": imageWidth
        });
    };
    PackeryService.prototype.packeryInit = function () {
        var grid = $('.grid');
        var self = this;
        grid.imagesLoaded(function () {
            self.setImageSize();
            grid.packery({
                // options
                itemSelector: '.grid-item',
                gutter: 0
            });
            // make all grid-items draggable
            grid.find('.grid-item').each(function (i, gridItem) {
                var draggie = new Draggabilly(gridItem);
                // bind drag events to Packery
                grid.packery('bindDraggabillyEvents', draggie);
            });
        });
        $(window).resize(this.setImageSize);
    };
    PackeryService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], PackeryService);
    return PackeryService;
}());
exports.PackeryService = PackeryService;
//# sourceMappingURL=packery.service.js.map