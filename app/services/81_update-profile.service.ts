/**
 * Created by Ksenia Koldaeva on 5/26/17.
 */

import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, RequestOptionsArgs, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from '../models/81_user.model';

@Injectable()
export class UpdateProfile {

  constructor(private http: Http) {}

  /* ======== Update Profile ========== */
  postUrl:string = '/users/update-profile';

  updateProfile(user:User, image:File): Observable<Response> {

    let formData: FormData = new FormData();
    formData.append('user-string', JSON.stringify(user));
    formData.append('profile-image', image);

    return this.http.post(this.postUrl, formData)
      .map(UpdateProfile.extractUser)
      .catch(UpdateProfile.handleError);
  }

  /* ======== Helpers ========== */

  private static extractUser(res: Response) {
    let body = res.json();
    return body.user || body;
  }

  private static handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
