import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams } from '@angular/http';

import { PostConfig } from '../globals/post.config';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from '../models/81_user.model';
import { Post } from '../models/post.model';

@Injectable()
export class PostService {

  constructor(private http: Http, private postConfig: PostConfig ) {}

  /* ======== Create Board or Post ========== */
  types = this.postConfig.types;

  postUrl:string = '/users/add-post';

  createBoard(user:User): Observable<Response>{
    let options = {
      userId: user.id,
      type: this.types[0]
    };

    return this.http.post(this.postUrl, options )
                    .map(PostService.extractPost)
                    .catch(PostService.handleError);
  }

  createPost(user:User): Observable<Response>{
    let options = {
      userId: user.id,
      type: this.types[1]
    };

    return this.http.post(this.postUrl, options)
                    .map(PostService.extractPost)
                    .catch(PostService.handleError);
  }

  /* ======== Get Posts ========== */

  getPosts(user:User) {

    let params: URLSearchParams = new URLSearchParams();
    params.set('userId', user.id );

    let requestOptions = new RequestOptions();
    requestOptions.search = params;


    return this.http.get('/users/posts', requestOptions )
                    .map( function( res: Response ){
                      return res.json();
                    })
                    .catch( PostService.handleError );
  }

  /* ======== Delete Board or Post ========== */

  deletePostUrl:string = '/users/delete-post';

  deletePost(post:Post): Observable<Response> {
    let options = {
      postId: post.id
    };

    return this.http.post(this.deletePostUrl, options)
                    .map(PostService.extractPost)
                    .catch(PostService.handleError);
  }
  /* ======== Helpers ========== */

  private static extractPost(res: Response) {
    let body = res.json();
    return body.post || body;
  }

  private static handleError (error: Response | any) {
      // TODO: check it out: In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
  }
}
