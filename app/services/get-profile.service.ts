import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response, } from '@angular/http';
import { HttpModule, JsonpModule } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


import { AuthService } from '../services/authentication.service';
import { User } from '../models/81_user.model';

@Injectable()
export class GetProfile {

  constructor(private http: Http, private auth: AuthService ) {}

  getProfile() {

    let headers = new Headers({ 'Authorization': 'Bearer '+ this.auth.getToken() });
    let options = new RequestOptions({ headers: headers });

    return this.http.get('/users/profile', options )
                    .map( function( res: Response ){
                      let user:User = res.json();
                      return user;
                    })
                    .catch(this.handleError);
  }

  private handleError (error: Response | any) {
      // TODO: check it out: In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
  }

}
