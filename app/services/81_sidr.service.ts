import { Injectable } from '@angular/core';
declare var $:any;
declare var sidr:any;

@Injectable()
export class SidrService {
  sidr:any;
  menuOpen:any;
  menuClose:any;

  sidrInit(){
    let self = this;
    self.sidr = $('#sidr');
    self.menuOpen = $('#menuOpen');
    self.menuClose = $('#menuClose');

    self.sidr.css('display', 'block');

    self.menuOpen.sidr({
        side: 'right',
        displace: false,
        onOpen: function() {
          self.menuOpen.css('display', 'none');
        },
        onClose: function() {
          self.menuOpen.css('display', 'block');
        }
    });

    self.menuClose.sidr({
        side: 'right',
        displace: false,
        onOpen: function() {
          self.menuOpen.css('display', 'none');
        },
        onClose: function() {
          self.menuOpen.css('display', 'block');
        }
    });
  }


  sidrExpand() {
    let self = this;
    self.sidr = $('#sidr');

    this.sidr.addClass('expanded');
  }

  sidrCollapse() {
    let self = this;
    self.sidr = $('#sidr');

    this.sidr.removeClass('expanded');
  }

}
