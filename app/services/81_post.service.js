"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var post_config_1 = require('../globals/post.config');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/map');
require('rxjs/add/operator/toPromise');
var PostService = (function () {
    function PostService(http, postConfig) {
        this.http = http;
        this.postConfig = postConfig;
        /* ======== Create Board or Post ========== */
        this.types = this.postConfig.types;
        this.postUrl = '/users/add-post';
        /* ======== Delete Board or Post ========== */
        this.deletePostUrl = '/users/delete-post';
    }
    PostService.prototype.createBoard = function (user) {
        var options = {
            userId: user.id,
            type: this.types[0]
        };
        return this.http.post(this.postUrl, options)
            .map(PostService.extractPost)
            .catch(PostService.handleError);
    };
    PostService.prototype.createPost = function (user) {
        var options = {
            userId: user.id,
            type: this.types[1]
        };
        return this.http.post(this.postUrl, options)
            .map(PostService.extractPost)
            .catch(PostService.handleError);
    };
    /* ======== Get Posts ========== */
    PostService.prototype.getPosts = function (user) {
        var params = new http_1.URLSearchParams();
        params.set('userId', user.id);
        var requestOptions = new http_1.RequestOptions();
        requestOptions.search = params;
        return this.http.get('/users/posts', requestOptions)
            .map(function (res) {
            return res.json();
        })
            .catch(PostService.handleError);
    };
    PostService.prototype.deletePost = function (post) {
        var options = {
            postId: post.id
        };
        return this.http.post(this.deletePostUrl, options)
            .map(PostService.extractPost)
            .catch(PostService.handleError);
    };
    /* ======== Helpers ========== */
    PostService.extractPost = function (res) {
        var body = res.json();
        return body.post || body;
    };
    PostService.handleError = function (error) {
        // TODO: check it out: In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    PostService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, post_config_1.PostConfig])
    ], PostService);
    return PostService;
}());
exports.PostService = PostService;
//# sourceMappingURL=81_post.service.js.map