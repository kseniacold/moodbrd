import { Component, EventEmitter, Output } from '@angular/core';

import { GetProfile } from '../services/get-profile.service';
import { UpdateProfile } from '../services/81_update-profile.service';
import { User } from '../models/81_user.model';
import {UserConfig} from '../globals/81_user.config';


@Component({
  selector: 'profile-settings',
  templateUrl: 'template/profile_preferences'
})

export class ProfileSettingsComponent {
  // new EventEmitter to be able to emit an event and propagate it to parent component
  @Output() onCancel = new EventEmitter<boolean>();

  // Initialize with empty object
  user: User = new User({});
  submitted = false;
  allowCommentsFor: Array<string> = this.userConfig.allowCommentsFor;
  tempImgSrc: string = '#';
  file: File;
  showConfirmation: boolean = false;

  constructor(private profileService: GetProfile, private updateProfile: UpdateProfile, private userConfig: UserConfig) {}

  // This executes
  ngOnInit(): void {
    let self = this;
    self.profileService
      .getProfile()
      .subscribe(
        data => {
          self.user = new User(data);
          self.tempImgSrc = self.user.profileImage;
        },
        error => {
          console.log(error);
        }
      );
  }


  onSubmit() {
    let self = this;
    this.submitted = true;
    this.updateProfile.updateProfile(this.user, this.file)
      .subscribe(
        data => {
          self.user = new User(data);
          self.showConfirmation = true;
        },
        error => {
          console.log(error);
        }
      );
  }

  previewFile(event: EventTarget) {
    let self = this;

    let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    let files: FileList = target.files;
    this.file = files[0];

    let reader = new FileReader();

    reader.onload = function (e: any) {
      self.tempImgSrc = e.target.result;
    };
    reader.readAsDataURL(this.file);
  }

  // propagating an event to a parent component that will collapse the sidebar
  cancelWindow(cancel: boolean) {
    cancel = true;
    this.onCancel.emit(cancel);
  }

  /* Diagnostic when needed
    get diagnostic() { return JSON.stringify(this.user); }
  */
}


