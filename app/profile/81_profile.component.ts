import { Component } from '@angular/core';
import { AuthService } from '../services/authentication.service';
import { GetProfile } from '../services/get-profile.service';
import { SidrService } from '../services/81_sidr.service';

import { User } from '../models/81_user.model';


@Component({
  selector: 'profile-menu',
  templateUrl: 'template/profile_menu'
})

export class ProfileComponent {
  // Initialize with an empty object
  user: User = new User({});
  showPostList: boolean = true;
  showProfileView: boolean = true;
  showProfileSettings: boolean = false;
  constructor(private auth: AuthService, private sidrService: SidrService, private profileService: GetProfile) {}

  // Available methods
  logout() {
    this.auth.logout();
  }

  // Manages the work with extendable sidebar
  expandSidebar() {
    this.sidrService.sidrExpand();
    this.showProfileSettings = true;
    this.showPostList = false;
    this.showProfileView = false;
  }

  collapseSidebar() {
    let self = this;
    this.sidrService.sidrCollapse();
    this.showProfileSettings = false;
    this.showPostList = true;
    this.showProfileView = true;

    // update profile when settings close
    self.profileService
      .getProfile()
      .subscribe(
        data => {
          self.user = new User(data);
        },
        error => {
          console.log(error);
        }
      );
  }

  toggleSidebar() {
    if (this.showProfileSettings) {
      this.collapseSidebar();
    } else {
      this.expandSidebar();
    }
  }

  // onCancel method listens for a child component to emit an event and handles it
  onCancel(cancel: boolean) {
    if (cancel) { this.collapseSidebar(); }
  }

  // This executes and populates user.
  ngOnInit(): void {
    let self = this;
    self.profileService
      .getProfile()
      .subscribe(
        data => {
          self.user = new User(data);
        },
        error => {
          console.log(error);
        }
      );
  }

}
