"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var authentication_service_1 = require('../services/authentication.service');
var get_profile_service_1 = require('../services/get-profile.service');
var _81_sidr_service_1 = require('../services/81_sidr.service');
var _81_user_model_1 = require('../models/81_user.model');
var ProfileComponent = (function () {
    function ProfileComponent(auth, sidrService, profileService) {
        this.auth = auth;
        this.sidrService = sidrService;
        this.profileService = profileService;
        // Initialize with an empty object
        this.user = new _81_user_model_1.User({});
        this.showPostList = true;
        this.showProfileView = true;
        this.showProfileSettings = false;
    }
    // Available methods
    ProfileComponent.prototype.logout = function () {
        this.auth.logout();
    };
    // Manages the work with extendable sidebar
    ProfileComponent.prototype.expandSidebar = function () {
        this.sidrService.sidrExpand();
        this.showProfileSettings = true;
        this.showPostList = false;
        this.showProfileView = false;
    };
    ProfileComponent.prototype.collapseSidebar = function () {
        var self = this;
        this.sidrService.sidrCollapse();
        this.showProfileSettings = false;
        this.showPostList = true;
        this.showProfileView = true;
        // update profile when settings close
        self.profileService
            .getProfile()
            .subscribe(function (data) {
            self.user = new _81_user_model_1.User(data);
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent.prototype.toggleSidebar = function () {
        if (this.showProfileSettings) {
            this.collapseSidebar();
        }
        else {
            this.expandSidebar();
        }
    };
    // onCancel method listens for a child component to emit an event and handles it
    ProfileComponent.prototype.onCancel = function (cancel) {
        if (cancel) {
            this.collapseSidebar();
        }
    };
    // This executes and populates user.
    ProfileComponent.prototype.ngOnInit = function () {
        var self = this;
        self.profileService
            .getProfile()
            .subscribe(function (data) {
            self.user = new _81_user_model_1.User(data);
        }, function (error) {
            console.log(error);
        });
    };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'profile-menu',
            templateUrl: 'template/profile_menu'
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthService, _81_sidr_service_1.SidrService, get_profile_service_1.GetProfile])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=81_profile.component.js.map