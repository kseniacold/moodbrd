"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var get_profile_service_1 = require('../services/get-profile.service');
var _81_update_profile_service_1 = require('../services/81_update-profile.service');
var _81_user_model_1 = require('../models/81_user.model');
var _81_user_config_1 = require('../globals/81_user.config');
var ProfileSettingsComponent = (function () {
    function ProfileSettingsComponent(profileService, updateProfile, userConfig) {
        this.profileService = profileService;
        this.updateProfile = updateProfile;
        this.userConfig = userConfig;
        // new EventEmitter to be able to emit an event and propagate it to parent component
        this.onCancel = new core_1.EventEmitter();
        // Initialize with empty object
        this.user = new _81_user_model_1.User({});
        this.submitted = false;
        this.allowCommentsFor = this.userConfig.allowCommentsFor;
        this.tempImgSrc = '#';
        this.showConfirmation = false;
    }
    // This executes
    ProfileSettingsComponent.prototype.ngOnInit = function () {
        var self = this;
        self.profileService
            .getProfile()
            .subscribe(function (data) {
            self.user = new _81_user_model_1.User(data);
            self.tempImgSrc = self.user.profileImage;
        }, function (error) {
            console.log(error);
        });
    };
    ProfileSettingsComponent.prototype.onSubmit = function () {
        var self = this;
        this.submitted = true;
        this.updateProfile.updateProfile(this.user, this.file)
            .subscribe(function (data) {
            self.user = new _81_user_model_1.User(data);
            self.showConfirmation = true;
        }, function (error) {
            console.log(error);
        });
    };
    ProfileSettingsComponent.prototype.previewFile = function (event) {
        var self = this;
        var eventObj = event;
        var target = eventObj.target;
        var files = target.files;
        this.file = files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            self.tempImgSrc = e.target.result;
        };
        reader.readAsDataURL(this.file);
    };
    // propagating an event to a parent component that will collapse the sidebar
    ProfileSettingsComponent.prototype.cancelWindow = function (cancel) {
        cancel = true;
        this.onCancel.emit(cancel);
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ProfileSettingsComponent.prototype, "onCancel", void 0);
    ProfileSettingsComponent = __decorate([
        core_1.Component({
            selector: 'profile-settings',
            templateUrl: 'template/profile_preferences'
        }), 
        __metadata('design:paramtypes', [get_profile_service_1.GetProfile, _81_update_profile_service_1.UpdateProfile, _81_user_config_1.UserConfig])
    ], ProfileSettingsComponent);
    return ProfileSettingsComponent;
}());
exports.ProfileSettingsComponent = ProfileSettingsComponent;
//# sourceMappingURL=81_profile-settings.component.js.map