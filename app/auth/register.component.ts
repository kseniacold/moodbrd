import { Component } from '@angular/core';
import { RegisterUser }    from '../models/register-user.model';
import { AuthService } from '../services/authentication.service';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'landing-register',
  templateUrl: 'template/landing_register'
})

export class RegisterComponent  {

  constructor( private authService: AuthService ){ }

  model = new RegisterUser('', '', '', '', '');
  submitted = false;

  onSubmit() {
    // TODO: Remove this when we're done
    console.log( JSON.stringify(this.model) );
    this.authService.register( this.model )
                    .subscribe(
                      data => {
                        //TODO: fix what to do on success!
                        window.location.replace('/');
                      },
                      error => {
                           // TODO: checkout why using console here, register failed so display error
                           console.log(error);
                      }
                    );

  }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }

}
