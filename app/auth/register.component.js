"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var register_user_model_1 = require('../models/register-user.model');
var authentication_service_1 = require('../services/authentication.service');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/map');
var RegisterComponent = (function () {
    function RegisterComponent(authService) {
        this.authService = authService;
        this.model = new register_user_model_1.RegisterUser('', '', '', '', '');
        this.submitted = false;
    }
    RegisterComponent.prototype.onSubmit = function () {
        // TODO: Remove this when we're done
        console.log(JSON.stringify(this.model));
        this.authService.register(this.model)
            .subscribe(function (data) {
            //TODO: fix what to do on success!
            window.location.replace('/');
        }, function (error) {
            // TODO: checkout why using console here, register failed so display error
            console.log(error);
        });
    };
    Object.defineProperty(RegisterComponent.prototype, "diagnostic", {
        // TODO: Remove this when we're done
        get: function () { return JSON.stringify(this.model); },
        enumerable: true,
        configurable: true
    });
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'landing-register',
            templateUrl: 'template/landing_register'
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthService])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map