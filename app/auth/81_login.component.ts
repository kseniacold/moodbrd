import { Component } from '@angular/core';
import { Credentials }    from './credentials';
import { Router } from '@angular/router';

import { AuthService } from '../services/authentication.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'landing-login',
  templateUrl: 'template/landing_login'
})

export class LoginComponent  {
  constructor( private authService: AuthService ){ }

  model = new Credentials('', '');
  submitted = false;

  onSubmit() {
    this.authService.login( this.model )
                    .subscribe(
                      data => {
                        window.location.reload();
                      },
                      error => {
                           //register failed so display an error
                           console.log(error);
                      }
                    );

  }
}
