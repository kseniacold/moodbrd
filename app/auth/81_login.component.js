"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var credentials_1 = require('./credentials');
var authentication_service_1 = require('../services/authentication.service');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/map');
var LoginComponent = (function () {
    function LoginComponent(authService) {
        this.authService = authService;
        this.model = new credentials_1.Credentials('', '');
        this.submitted = false;
    }
    LoginComponent.prototype.onSubmit = function () {
        this.authService.login(this.model)
            .subscribe(function (data) {
            window.location.reload();
        }, function (error) {
            //register failed so display an error
            console.log(error);
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'landing-login',
            templateUrl: 'template/landing_login'
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=81_login.component.js.map