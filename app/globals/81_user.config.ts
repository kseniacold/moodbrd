import { Injectable } from '@angular/core';

@Injectable()
export class UserConfig {

  constructor() {}

  /**
   * Creating a small injectable service to be able to access user configuration value from anywhere.
   * @type {[string,string,string]}
   */
  allowCommentsFor = [
    'everyone',
    'people you follow',
    'disable comments'
  ];
}
