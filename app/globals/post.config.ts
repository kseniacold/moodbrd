import { Injectable } from '@angular/core';

@Injectable()
export class PostConfig {

  constructor() {}

  types = [
    'Board',
    'Post'
  ];

  statuses =[
    'draft',
    'published',
    'removed'
  ];

  allowCommentsFor = [
    'everyone',
    'people you follow',
    'disable comments'
  ];

}
