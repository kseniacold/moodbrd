var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs   = require('fs');
var less = require('less');
var lessMiddleware = require('less-middleware');

// Require Passport
var passport = require('passport');
var mongo = require('mongodb');
// Bring in the data model
var db =  require('./moodbrd_api/models/db');

var index = require('./routes/index');
var users = require('./moodbrd_api/routes/users');
var uploads = require('./moodbrd_api/routes/81_uploads');


//=============== PASSPORT MODULE. BRING DATA =================


// Bring in the Passport config after model is defined
var passportConfig = require('./moodbrd_api/config/passport');
// [SH] Bring in the routes for the API (delete the default routes)
var app = express();

//=============== PUG TEMPLATE ENGINE WITH DYNAMIC PATH CHANGE =================
// view engine setup
app.set('views', [ path.join(__dirname, 'views'),  path.join(__dirname, 'views/partials'),path.join(__dirname, 'views/users'), path.join(__dirname, 'views/posts') ]);
app.locals.basedir = path.join(__dirname, 'views');
app.set('view engine', 'pug');


//=============== EXPRESS DEFAULTS=================
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


//=============== LESS MIDDLEWARE =================

var lessHatPath = path.join(__dirname, 'node_modules', 'lesshat');
app.use(lessMiddleware(path.join(__dirname, 'less_source'), {
  dest: path.join(__dirname, 'public'),
  preprocess: {
    path: function(pathname, req) {
      return pathname.replace(path.sep + 'stylesheets' + path.sep, path.sep);
    },
    render: {
      paths: [ lessHatPath, path.join(__dirname, 'less_source')]
    }
  }
}));

//=============== STATIC PATHS =================
app.use(express.static(path.join(__dirname, 'public')));
app.use('/public', express.static(__dirname + '/public'));
app.use('/scripts', express.static(__dirname + '/node_modules'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));
app.use('/app', express.static(__dirname + '/app'));
app.use('/config', express.static(__dirname));

//=============== ROUTES =================

app.use('/', index);

// Initialize Passport before using the route middleware
app.use(passport.initialize());
// Use the moodbrd_api routes when path starts with /users
app.use('/users', users);
app.use('/uploads', uploads);


//=============== ERRORS =================

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  console.log('404 Error handler');
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
app.use(function(err, req, res, next) {
  console.log('Some Error handler. env = ', req.app.get('env') , 'err=', err);

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  try {
    res.render('error');
  } catch(e) {
    console.log('Error rendering error page :-/ ', e);
  }

  // Ivan is smart, Ivan reads documentation, Ivan adds following line...
  next(err);
  // see https://expressjs.com/en/guide/error-handling.html for details
});

// Catch unauthorised errors
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});

module.exports = app;
