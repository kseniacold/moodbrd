var mongoose = require('mongoose');
var User = mongoose.model('User');

function profileRead(req, res) {
  if (!req.payload) {
    res.status(401).json({
      "message" : "err: " + JSON.stringify(req)
    });
  } else if (!req.payload._id) {
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    User
      .findById(req.payload._id)
      .exec(function(err, user) {
        res.status(200).json(user);
      });
  }
}

function profileUpdate(req, res) {
  var user,
      userId,
      file,
      filePath;

  if (!req.body) {
    res.status(401).json({
      "message" : "err: " + JSON.stringify(req)
    });
  } else {
    user = JSON.parse(req.body['user-string']);
    userId = user.__id.valueOf();

    if(req.files['profile-image']) {
      file = req.files['profile-image'][0];
      filePath = file.path.replace("public/", "");
      //Update object from request to reflect the new path
      user._profileImage = filePath;
    }

    //Update user in DB
    User.findById(userId, function (err, userUpd){
      if (err) {
        res.send(422,'update failed');
      } else {
        //update fields
        for (var field in User.schema.paths) {
          if ((field !== '_id') && (field !== '__v')) {
            if (user['_' + field] !== undefined) {
              userUpd[field] = user['_' + field];
            }
          }
        }
        userUpd.save();
        res.status(200).json(userUpd);
      }
    });
  }
}

function uploadImage(req, res) {
  res.json({
    success: true,
    message: 'Image uploaded'
  });
}

module.exports = {
  profileRead: profileRead,
  profileUpdate: profileUpdate,
  uploadProfileImage: uploadImage
};
