(function(){

var mongoose = require('mongoose');
var Post = mongoose.model('Post');

/**
  * handleAddPost() function is an end-point for adding a post.
  * Accepts request obj. with "userId" and "type" properties
  * Returns response obj. containing a post object.
*/
function handleAddPost(req, res) {
  var userId    = req.body.userId,
      postType  = req.body.type;
  getUserPosts(
    userId,
    function(posts){
      var title = setTitle(postType, posts);
      addPost(
        userId,
        postType,
        title,
        function(post) {
          res.status(200);
          res.json({
            "post" : post
          });
        }
      );
    }
  );
}

/**
  * handleGetPost() function is an end-point for getting user's posts.
  *
*/
function handleGetPosts(req, res) {
  Post
    .find({ creator: req.query.userId })
    .exec(function (err, posts ) {
      if (err) {
        console.log( "Post query error: " + err );
      } else {
        res.status(200).json(posts);
      }
    });
}

/**
  * deletePost() function sets post's status to "deleted".
  * It is an end-point function
  *
  *
*/
function deletePost(req, res) {
  var postId = req.body.postId,
      query = {"_id": postId.valueOf() },
      update = { $set: { status: "deleted" } },
      options = { new: true };

  Post.findByIdAndUpdate(query, update, options, function(err, post) {
    if (err) {
      console.log( "Post query error: " + err );
    } else {
      res.status(200).json(post);
    }
  });
}

/**
  * addPost() function adds a reference to the post into user's "posts" property.
  * It invokes provided callback after getting user from DB.
*/
function addPost(userId, type, title, callbackFn) {
  var post = new Post({
    creator: userId,
    type:     type,
    title:    title
  });

  post.save(function(err, post) {
    var postId = post._id.valueOf();
    //update user with postId reference
    //get User with following query
    var query = {"_id": userId.valueOf() },
        update = { $push: { posts: postId } },
        options = { new: true };

    User.findOneAndUpdate(query, update, options, function(err, user) {
      if (err) throw new Error("User query error: ");
      callbackFn(post);
    });
  });
}

/**
  * getUserPosts() gets posts by user with a provided callback function.
  *
*/
function getUserPosts(userId, callbackFn)
{
  Post.find({ creator: userId })
    .exec(function (err, posts) {
      if (err) throw new Error("Post query error: " + err);
      callbackFn(posts);
    });
}

/**
  * setTitle() function sets the title for newly created Posts or Boards.
  *
*/
function setTitle(type, posts) {
  var title;
  var max = 1;

  if(posts.length) {
    posts.forEach( function( post ){
      var matches = /(Board|Post) (\d+)/.exec( post.title );

      if( matches != null )
      {
        //get the digit in the matches
        var num = parseInt( matches[2] );
        if( num > max)
        {
          max = num;
        }
      }
    });

    switch(type) {
      case 'Board':
        title = 'Board ' + (max + 1);
        break;
      case 'Post':
        title = 'Post ' + (max + 1);
        break;
    }
  } else { //If there is no Posts or Boards yet, create the first one
    switch(type) {
      case 'Board':
        title = 'Board ' + 1;
        break;
      case 'Post':
        title = 'Post ' + 1;
        break;
    }
  }
  return title;
}

module.exports = {
  addPost:        handleAddPost,
  getPostsByUser: handleGetPosts,
  deletePost:     deletePost
};

})();
