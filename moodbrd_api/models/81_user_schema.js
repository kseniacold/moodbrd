var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
// var appSecret = process.env.MOODBRD_APP_SLT;
//TODO: remove the secret from the code!NB
var appSecret = 'MOODBRDSECRET';


var userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  encryptedPassword: String,
  salt: String,
  posts: [ mongoose.Schema.Types.ObjectId ],
  personalWebsite: String,
  bio: String,
  profileImage: {
    type: String,
    default: 'defaults/default-profile-image.jpg'
  },
  followsUsers: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  followedByUsers: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  blockedUsers: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  likedElements: [ mongoose.Schema.Types.ObjectId ],
  allowCommentsFor: String
});


// Declaring methods for setting and validating passwords
userSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.encryptedPassword = crypto.pbkdf2Sync(password, this.salt, 1000, 256, 'sha256').toString('hex');
  console.log( this.encryptedPassword );
};

userSchema.methods.validPassword = function(password) {
  var encryptedPassword = crypto.pbkdf2Sync(password, this.salt, 1000, 256, 'sha256').toString('hex');
  return this.encryptedPassword === encryptedPassword;
};

//Method for generating web token
userSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    exp: parseInt(expiry.getTime() / 1000)
  }, appSecret);
};

mongoose.model('User', userSchema);
