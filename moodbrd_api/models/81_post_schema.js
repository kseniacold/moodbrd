var mongoose = require( 'mongoose' );

var postSchema = new mongoose.Schema({
  creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  type: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    default: "draft"
  },
  updated: {
    type: Date,
    default: Date.now
  },
  allowCommentsFor: {
    type: String,
    required: true,
    default: "everyone"
  },
  featured: {
    type: Boolean,
    required: true,
    default: false
  },
  title: {
    type: String,
    default: ""
  },
  description: {
    type: String,
    default: ""
  },
  likes: [ mongoose.Schema.Types.ObjectId ],
  comments: [ mongoose.Schema.Types.ObjectId ],
  tags: [ mongoose.Schema.Types.ObjectId ],
  collaborators: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

mongoose.model('Post', postSchema);
