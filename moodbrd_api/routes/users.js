var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var multer = require('multer'); //node.js middleware for handling file uploads.
//var appSecret = process.env.MOODBRD_APP_SLT;
var appSecret = 'MOODBRDSECRET';
var auth = jwt({
  secret: appSecret,
  userProperty: 'payload'
});


var ctrlProfile = require('../controllers/81_profile');
var ctrlAuth = require('../controllers/authentication');

var ctrlPost = require('../controllers/81_post');

//TODO: Remove AJAX windowsAJAX windows below
/* GET Signup window for AJAX request */
router.get('/modal-signup', function(req, res) {
  res.render('users/modal-signup', {layout: false});
});

/* GET Signin window for AJAX request */
router.get('/modal-signin', function(req, res) {
  res.render('users/modal-signin', {layout: false});
});

// profile
router.get('/profile', auth, ctrlProfile.profileRead );


/* =============== update Profile with profile image update =====================*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/assets')
  },
  //TODO: handle different MIME types for different extension
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
});

var upload = multer({ storage: storage });

router.post('/update-profile', upload.fields([{ name: 'user-string', maxCount: 1}, { name: 'profile-image', maxCount: 1 }]), ctrlProfile.profileUpdate);
/* =============== end update Profile  =====================*/


// authentication
router.post('/register', ctrlAuth.register );
router.post('/login', ctrlAuth.login );

//addPost
router.post('/add-post', ctrlPost.addPost );

//deletePost
router.post('/delete-post', ctrlPost.deletePost );

//posts
router.get('/posts', ctrlPost.getPostsByUser );

module.exports = router;
